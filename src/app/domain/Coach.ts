export default class Coach {

  constructor(public name: string, public speciality: string, public age: any, public phone: string, public mail: string, public img: string) {
  }

}
