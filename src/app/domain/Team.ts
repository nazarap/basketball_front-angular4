import Player from "./Player";

export default class Team {

  constructor(public name?: string, public id?: number, public teams?: Array<Player>) {
  }

}
