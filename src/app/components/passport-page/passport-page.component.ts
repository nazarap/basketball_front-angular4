import { Component, OnInit } from '@angular/core';
import Passport from "../../domain/Passport";
import Coach from "../../domain/Coach";
import {ApiService} from "../../services/api.service";

@Component({
  selector: 'app-passport-page',
  providers: [ApiService],
  templateUrl: './passport-page.component.html',
  styleUrls: ['./passport-page.component.css']
})
export class PassportPageComponent implements OnInit {
  PasportList: Array<Passport>;
  age: number;
  open: boolean;
  sex: string;
  sexList = [
    'Чоловіча стать',
    'Жіноча стать'
  ];

  constructor(private apiService: ApiService) {
  }

  ngOnInit() {
    this.apiService.passports().subscribe(
      data => {
        this.PasportList = data as Passport[];
      },
      error => {},
      () => {}
    );
  }

  addNewPasport = function () {

  }

}
