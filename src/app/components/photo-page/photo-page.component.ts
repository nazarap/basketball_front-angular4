import { Component, OnInit } from '@angular/core';
import Image from "../../domain/Image";
import Video from "../../domain/Video";
import {ApiService} from "../../services/api.service";

@Component({
  selector: 'app-photo-page',
  templateUrl: './photo-page.component.html',
  providers: [ApiService],
  styleUrls: ['./photo-page.component.css']
})
export class PhotoPageComponent implements OnInit {
  OpenImgList: Array<Image>;
  addPhotoModal = {
    open: false,
    image: null
  };

  constructor(private apiService: ApiService) {
  }

  ngOnInit() {
    this.getPhotos();
  }

  getPhotos() {
    this.apiService.photos().subscribe(
      data => {
        this.OpenImgList = data as Image[];
      },
      error => {},
      () => {}
    );
  }

  getCredentials(): boolean {
    return !!sessionStorage.getItem("authToken");
  }

  createPhoto = function() {
    this.apiService.addPhoto({image: this.addPhotoModal.image}).subscribe(
      data => {
        this.addPhotoModal.open = false;
        this.getPhotos();
      },
      error => {},
      () => {}
    )
  };

  deletePhoto(id) {
    this.apiService.deletePhoto(id).subscribe(
      data => {
        this.getPhotos();
      },
      error => {},
      () => {}
    )
  }

  open(image: string) {
    window.open(image);
  }

  handleFileSelect(evt){
    let files = evt.target.files;
    let file = files[0];

    if (files && file) {
      let reader = new FileReader();

      reader.onload =this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    let binaryString = readerEvt.target.result;
    this.addPhotoModal.image = btoa(binaryString);
  }
}
