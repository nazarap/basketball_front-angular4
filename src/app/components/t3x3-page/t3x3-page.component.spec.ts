import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { T3x3PageComponent } from './t3x3-page.component';

describe('T3x3PageComponent', () => {
  let component: T3x3PageComponent;
  let fixture: ComponentFixture<T3x3PageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ T3x3PageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(T3x3PageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
