import { Component, OnInit } from '@angular/core';
import News from "../../domain/News";
import {ApiService} from "../../services/api.service";

@Component({
  selector: 'app-calendar-page',
  templateUrl: './calendar-page.component.html',
  providers: [ApiService],
  styleUrls: ['./calendar-page.component.css']
})
export class CalendarPageComponent implements OnInit {

  newsMoreList: Array<News>;

  showNewsModal: any = {
    open: false,
    selected: null
  };

  constructor(private apiService: ApiService) {}

  ngOnInit() {
    this.apiService.news().subscribe(
      data => {this.newsMoreList = data as News[]},
      error => {},
      () => {}
    );
  }

  deleteNews(id, index): void {
    this.newsMoreList.splice(index, 1);
    this.apiService.deleteNews(id).subscribe(
      data => {
      },
      error => {},
      () => {}
    );
  }

  getCredentials(): boolean {
    return !!sessionStorage.getItem("authToken");
  }

  NewsMore = function(index) {
    // var modalInstance = $uibModal.open({
    //   templateUrl: 'templates/news_more.html',
    //   size: 'lg',
    //   controller: 'NewsMoreCtrl',
    //   controllerAs: '$ctrl',
    //   resolve: {
    //     news: function () {
    //       return cv.newsMoreList[index];
    //     }
    //   }
    // });
    //
    // modalInstance.result.then(function (news) {
    //   cv.newsMoreList.unshift(news);
    // }, function () {});
  };

}
