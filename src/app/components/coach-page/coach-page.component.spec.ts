import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoachPageComponent } from './coach-page.component';

describe('CoachPageComponent', () => {
  let component: CoachPageComponent;
  let fixture: ComponentFixture<CoachPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoachPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoachPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
