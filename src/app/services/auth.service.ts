import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers } from '@angular/http';
import { BASE_URL, AUTH } from '../app.config';

@Injectable()
export class AuthService {
    private headers: Headers;

    constructor(private http: Http) {
        this.headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        this.headers.append('Authorization', AUTH);
    }

    login(login: string, password: string): any {
        let body = `grant_type=password&username=${login}&password=${password}`;
        return this.http
           .post(`${BASE_URL}oauth/token`, body, { headers: this.headers })
           .map(res => res.json());
    }
}
