import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutStylesComponent } from './components/about-styles/about-styles.component';
import { ActionBarComponent } from './components/action-bar/action-bar.component';
import { AdminBarComponent } from './components/admin-bar/admin-bar.component';
import { AdminContentComponent } from './components/admin-content/admin-content.component';
import { AdminLoginComponent } from './components/admin-login/admin-login.component';
import { AdminStyleComponent } from './components/admin-style/admin-style.component';
import { AdminSubtypeComponent } from './components/admin-subtype/admin-subtype.component';
import { AdminTypeComponent } from './components/admin-type/admin-type.component';
import { ListBlockComponent } from './components/list-block/list-block.component';
import { SearchBlockComponent } from './components/search-block/search-block.component';
import { SearchContentComponent } from './components/search-content/search-content.component';
import { StylePageComponent } from './components/style-page/style-page.component';
import { TypeModalComponent } from './components/type-modal/type-modal.component';
import { TinyComponent } from './tiny.component';
import {TruncatePipe} from './filters/truncate';


import { MainPageComponent } from './components/main-page/main-page.component';
import { T3x3PageComponent } from './components/t3x3-page/t3x3-page.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { CoachPageComponent } from './components/coach-page/coach-page.component';
import { CalendarPageComponent } from './components/calendar-page/calendar-page.component';
import { NewsMoreComponent } from './components/news-more/news-more.component';
import { PassportPageComponent } from './components/passport-page/passport-page.component';
import { PhotoPageComponent } from './components/photo-page/photo-page.component';
import { TeamPageComponent } from './components/team-page/team-page.component';
import { VideoPageComponent } from './components/video-page/video-page.component';

@NgModule({
  declarations: [
    AppComponent,
    AboutStylesComponent,
    ActionBarComponent,
    AdminBarComponent,
    AdminContentComponent,
    AdminLoginComponent,
    AdminStyleComponent,
    AdminSubtypeComponent,
    AdminTypeComponent,
    ListBlockComponent,
    SearchBlockComponent,
    SearchContentComponent,
    StylePageComponent,
    TypeModalComponent,
    TruncatePipe,
    TinyComponent,
    MainPageComponent,
    T3x3PageComponent,
    LoginPageComponent,
    CoachPageComponent,
    CalendarPageComponent,
    NewsMoreComponent,
    PassportPageComponent,
    PhotoPageComponent,
    TeamPageComponent,
    VideoPageComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'art.style'}),
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
