import { Injectable } from '@angular/core';
import {AUTH, BASE_URL} from "../app.config";
import {Headers, Http} from "@angular/http";
import News from "../domain/News";
import Image from "../domain/Image";
import 'rxjs/add/operator/map'

@Injectable()
export class ApiService {
  headersGet: Headers;
  headersPost: Headers;
  constructor(private http: Http) {
    this.headersGet = new Headers({'Authorization': AUTH});
    this.headersPost = new Headers({'Authorization': `bearer ${sessionStorage.getItem("authToken")}`});
  }

  news(): any {
    return this.http
      .get(`${BASE_URL}api/news`, { headers: this.headersGet })
      .map(res => res.json());
  }

  newsGame(): any {
    return this.http
      .get(`${BASE_URL}api/news/category/GAME`, { headers: this.headersGet })
      .map(res => res.json());
  }

  newsGeneral(): any {
    return this.http
      .get(`${BASE_URL}api/news/category/GENERAL`, { headers: this.headersGet })
      .map(res => res.json());
  }

  advertisement(): any {
    return this.http
      .get(`${BASE_URL}api/advertisement`, { headers: this.headersGet })
      .map(res => res.json());
  }

  sponsors(): any {
    return this.http
      .get(`${BASE_URL}api/sponsors`, { headers: this.headersGet })
      .map(res => res.json());
  }

  addSponsors(sponsor: Image): any {
    return this.http
      .post(`${BASE_URL}api/sponsors`, sponsor, { headers: this.headersPost })
      .map(res => res.json());
  }

  deleteSponsors(id: any): any {
    return this.http
      .delete(`${BASE_URL}api/sponsors/${id}`, { headers: this.headersPost })
      .map(res => res.json());
  }

  coaches(): any {
    return this.http
      .get(`${BASE_URL}api/trainers`, { headers: this.headersGet })
      .map(res => res.json());
  }

  passports(): any {
    return this.http
      .get(`${BASE_URL}api/passports`, { headers: this.headersGet })
      .map(res => res.json());
  }

  videos(): any {
    return this.http
      .get(`${BASE_URL}api/videos`, { headers: this.headersGet })
      .map(res => res.json());
  }

  photos(): any {
    return this.http
      .get(`${BASE_URL}api/cheerleaders`, { headers: this.headersGet })
      .map(res => res.json());
  }

  addNews(news: News): any {
    return this.http
      .post(`${BASE_URL}api/news`, news, { headers: this.headersPost })
      .map(res => res.json());
  }

  deleteNews(id: any): any {
    return this.http
      .delete(`${BASE_URL}api/news/${id}`, { headers: this.headersPost })
      .map(res => res.json());
  }

  addPhoto(image: Image): any {
    return this.http
      .post(`${BASE_URL}api/cheerleaders`, image, { headers: this.headersPost })
      .map(res => res.json());
  }

  deletePhoto(id: any): any {
    return this.http
      .delete(`${BASE_URL}api/cheerleaders/${id}`, { headers: this.headersPost })
      .map(res => res.json());
  }
}
