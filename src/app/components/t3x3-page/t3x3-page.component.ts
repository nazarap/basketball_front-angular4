import { Component, OnInit } from '@angular/core';
import Team from "../../domain/Team";
import Leagues from "../../domain/Leagues";

@Component({
  selector: 'app-t3x3-page',
  templateUrl: './t3x3-page.component.html',
  styleUrls: ['./t3x3-page.component.css']
})
export class T3x3PageComponent implements OnInit {
  selectedTournament: Team = {};

  tournaments: Array<Leagues>;
  constructor() {
    this.tournaments = [
      {
        name: 'Winter League',
        id: 1, children: [
          {
            name: 'Grizzlies (Чоловіки)', id: 1,
            teams:         [
              {name: 'Petrov', number: 11, img: 'img-url', level: 'FG',lastname: 'Marusiak',color: 'green'},
              {name: 'Pedro', number: 23, img: 'img-url', level: 'F',lastname: 'Marusiak'},
              {name: 'Petka', number: 31, img: 'img-url', level: 'C',lastname: 'Marusiak'},
              {name: 'Petrov', number: 17, img: 'img-url', level: 'FG',lastname: 'Marusiak'},
            ]
          },
          {
            name: 'Франківськ (Жінки)', id: 2,
            teams:         [
              {name: 'Petrov', number: 11, img: 'img-url', level: 'FG',lastname: 'Marusiak',color: 'white'},
              {name: 'Pedro', number: 23, img: 'img-url', level: 'F',lastname: 'Marusiak'},
              {name: 'Petka', number: 31, img: 'img-url', level: 'C',lastname: 'Marusiak'},
              {name: 'Petrov', number: 17, img: 'img-url', level: 'FG',lastname: 'Marusiak'},
            ]
          }
        ]
      },
      {
        name: 'Summer League',
        id: 2, children: [
          {
            name: 'Grizzlies (Чоловіки)', id: 1,
            teams: [
            ]
          },
          {
            name: 'Франківськ (Жінки)', id: 2,
            teams: [
            ]
          }
        ]
      },
      {
        name: 'Summer League (U-18)',
        id: 3, children: [
          {
            name: 'Grizzlies (Чоловіки)', id: 1,
            teams: [
            ]
          },
          {
            name: 'Франківськ (Жінки)', id: 2,
            teams: [
            ]
          }
        ]
      },
      {
        name: 'Winter League (U-18)',
        id: 4, children: [
          {
            name: 'Grizzlies (Чоловіки)', id: 1,
            teams: [
            ]
          },
          {
            name: 'Франківськ (Жінки)', id: 2,
            teams: [
            ]
          }
        ]
      }
    ]
  }

  ngOnInit() {
  }

  deleteCardMore = function (index) {
    this.teamList.splice(index, 1)
  };

  addNewCard = function() {
    // var modalInstance = $uibModal.open({
    //   templateUrl: 'templates/add-card.html',
    //   size: 'lg',
    //   controller: 'AddCardCtrl',
    //   controllerAs: '$ctrl'
    // });
    //
    // modalInstance.result.then(function (cardNews) {
    //   cv.teamList.unshift(cardNews);
    // }, function () {});
  };

  getCredentials(): boolean {
    return !!sessionStorage.getItem("authToken");
  }

}
