import { Component, OnInit } from '@angular/core';
import Video from "../../domain/Video";
import Coach from "../../domain/Coach";
import {ApiService} from "../../services/api.service";

@Component({
  selector: 'app-video-page',
  templateUrl: './video-page.component.html',
  providers: [ApiService],
  styleUrls: ['./video-page.component.css']
})
export class VideoPageComponent implements OnInit {
  videos: Array<Video>;

  constructor(private apiService: ApiService) {}

  ngOnInit() {
    this.apiService.videos().subscribe(
      data => {
        this.videos = data as Video[];
      },
      error => {},
      () => {}
    );
  }

}
