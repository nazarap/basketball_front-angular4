import { Component, OnInit } from '@angular/core';
import Leagues from "../../domain/Leagues";
import Team from "../../domain/Team";

@Component({
  selector: 'app-team-page',
  templateUrl: './team-page.component.html',
  styleUrls: ['./team-page.component.css']
})
export class TeamPageComponent implements OnInit {
  selectedLeague: Team;

  leagues: Array<Leagues>;
  constructor() {
    this.selectedLeague = {};
    this.leagues = [
      {
        name: 'Вища Ліга',
        id: 1, children: [
          {
            name: 'Говерла (Чоловіки)',
            id: 1,
            teams: [
              {name: 'Petrov', number: 11, img: 'img-url', level: 'FG',lastname: 'Marusiak',color: 'green'},
              {name: 'Pedro', number: 23, img: 'img-url', level: 'F',lastname: 'Marusiak'},
              {name: 'Petka', number: 31, img: 'img-url', level: 'C',lastname: 'Marusiak'},
              {name: 'Petrov', number: 17, img: 'img-url', level: 'FG',lastname: 'Marusiak'},
              {name: 'Pedro', number: 28, img: 'img-url', level: 'F',lastname: 'Marusiak'},
              {name: 'Petrov', number: 1, img: 'img-url', level: 'FG',lastname: 'Marusiak'},
              {name: 'Pedro', number: 4, img: 'img-url', level: 'F',lastname: 'Marusiak'},
              {name: 'Pedro', number: 23, img: 'img-url', level: 'F',lastname: 'Marusiak'},
              {name: 'Petka', number: 31, img: 'img-url', level: 'C',lastname: 'Marusiak'},
              {name: 'Petrov', number: 17, img: 'img-url', level: 'FG',lastname: 'Marusiak'},
              {name: 'Pedro', number: 28, img: 'img-url', level: 'F',lastname: 'Marusiak'},
              {name: 'Petrov', number: 1, img: 'img-url', level: 'FG',lastname: 'Marusiak'},
              {name: 'Pedro', number: 4, img: 'img-url', level: 'F',lastname: 'Marusiak'}
            ]
          },
          {
            name: 'Франківськ (Жінки)',
            id: 2,
            teams: [
              {name: 'Nazar', number: 1, img: 'img-url', level: 'FG'},
              {name: 'Nazar2', number: 2, img: 'img-url', level: 'F'},
              {name: 'Nazar3', number: 3, img: 'img-url', level: 'C'}
            ]
          }
        ]
      },
      {
        name: 'Перша Ліга',
        id: 2, children: [
          {
            name: 'Франквськ (Чоловіки)',
            id: 1,
            teams: [
              {name: 'Nazar', number: 1, img: 'img-url', level: 'FG'},
              {name: 'Nazar2', number: 2, img: 'img-url', level: 'F'},
              {name: 'Nazar3', number: 3, img: 'img-url', level: 'C'}
            ]
          }
        ]
      },
      {
        name: 'Студентська Ліга',
        id: 3, children: [
          {
            name: 'ПНУ (Чоловіки)',
            id: 1
          },
          {
            name: 'ПНУ (Жінки)',
            id: 2
          }
        ]
      },
      {
        name: 'Summer league',
        id: 4, children: [
          {
            name: 'Франківськ (Чоловіки)',
            id: 1
          },
          {
            name: 'Калуш (Чоловіки)',
            id: 2
          }
        ]
      },
      {
        name: 'ВЧІФ',
        id: 5, children: [
          {
            name: 'Франківськ (Чоловіки)',
            id: 1
          },
          {
            name: 'Калуш (Чоловіки)',
            id: 2
          }
        ]
      },
      {
        name: 'ЮБЛ',
        id: 7, children: [
          {
            name: 'Говерла (Чоловіки)',
            id: 1
          }
        ]
      },
    ]
  }

  ngOnInit() {
  }

  getCredentials(): boolean {
    return !!sessionStorage.getItem("authToken");
  }

  changeTeam = function(type){
    this.selectedLeague = type;
  };

  deleteCardMore = function (index) {
    this.teamList.splice(index, 1)
  };

  addNewCard = function() {
    // var modalInstance = $uibModal.open({
    //   templateUrl: 'templates/add-card.html',
    //   size: 'lg',
    //   controller: 'AddCardCtrl',
    //   controllerAs: '$ctrl'
    // });
    //
    // modalInstance.result.then(function (cardNews) {
    //   cv.teamList.unshift(cardNews);
    // }, function () {});
  };
}
