import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchContentComponent } from './components/search-content/search-content.component';
import { AdminContentComponent } from './components/admin-content/admin-content.component';
import { AdminLoginComponent } from './components/admin-login/admin-login.component';
import { AdminStyleComponent } from './components/admin-style/admin-style.component';
import { AdminTypeComponent } from './components/admin-type/admin-type.component';
import { AdminSubtypeComponent } from './components/admin-subtype/admin-subtype.component';
import { AboutStylesComponent } from './components/about-styles/about-styles.component';
import { StylePageComponent } from './components/style-page/style-page.component';
import { TokenGuard } from './guard/token.guard';
import { MainPageComponent } from './components/main-page/main-page.component';
import { T3x3PageComponent } from './components/t3x3-page/t3x3-page.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { CoachPageComponent } from './components/coach-page/coach-page.component';
import { CalendarPageComponent } from './components/calendar-page/calendar-page.component';
import { NewsMoreComponent } from './components/news-more/news-more.component';
import { PassportPageComponent } from './components/passport-page/passport-page.component';
import { PhotoPageComponent } from './components/photo-page/photo-page.component';
import { TeamPageComponent } from './components/team-page/team-page.component';
import { VideoPageComponent } from './components/video-page/video-page.component';

const routes: Routes = [
  { path: '', component: MainPageComponent },

  { path: '3x3', component: T3x3PageComponent },
  { path: 'login', component: LoginPageComponent },
  { path: 'coach', component: CoachPageComponent },
  { path: 'news', component: CalendarPageComponent },
  { path: 'passport', component: PassportPageComponent },
  { path: 'photo', component: PhotoPageComponent },
  { path: 'team', component: TeamPageComponent },
  { path: 'video', component: VideoPageComponent },
  // { path: 'search', component: SearchContentComponent },
  // { path: 'styles', component: AboutStylesComponent },
  // { path: 'style/:link', component: StylePageComponent },
  // { path: 'admin', component: AdminContentComponent,
  //   children: [
  //     { path: '', redirectTo: 'login', pathMatch: 'full' },
  //     { path: 'login', component: AdminLoginComponent },
  //     { path: 'create/style', component: AdminStyleComponent, canActivate: [TokenGuard] },
  //     { path: 'create/type', component: AdminTypeComponent, canActivate: [TokenGuard] },
  //     { path: 'create/subtype', component: AdminSubtypeComponent, canActivate: [TokenGuard] }
  //   ]
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [TokenGuard]
})
export class AppRoutingModule { }
