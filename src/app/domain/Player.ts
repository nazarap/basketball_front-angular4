export default class Player {

  constructor(public name: string, public img?: string,
              public level?: string, public lastname?: string, public color?: any, public number?: number) {
  }

}
